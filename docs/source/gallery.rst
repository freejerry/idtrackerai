Some videos tracked with idtracker.ai
=====================================

Large groups of zebrafish (*D. rerio*)
*********************************

Videos of 60 and 100 30dpf zebrafish in a 70cm circular arena. For the setup specifications check Supplementary Figure 3 in [1]_.

.. raw:: html

  <iframe width="280" height="160" style="padding:2px;border:2px solid white;" style="padding:2px;border:2px solid white;" src="https://www.youtube.com/embed/Imz3xvPsaEw?ecver=1&rel=0&showinfo=01" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

.. raw:: html

    <iframe width="280" height="160" style="padding:2px;border:2px solid white;" src="https://www.youtube.com/embed/daSNVpJJBGE?ecver=&rel=0&showinfo=01" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

.. raw:: html

    <iframe width="280" height="160" style="padding:2px;border:2px solid white;" src="https://www.youtube.com/embed/Ry7nFjgNcX0?ecver=1&rel=0&showinfo=01" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

.. raw:: html

    <iframe width="280" height="160" style="padding:2px;border:2px solid white;" src="https://www.youtube.com/embed/nb5sUEUlpVs?ecver=1&rel=0&showinfo=01" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Small groups of zebrafish (*D. rerio*)
*********************************

Video of 8 adult zebrafish with complex behaviors.

.. raw:: html

  <iframe width="280" height="160" style="padding:2px;border:2px solid white;" src="https://www.youtube.com/embed/PdKpJEo9Thw?ecver=1&rel=0&showinfo=01" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Videos of 10 30dpf zebrafish in a 70cm circular arena.

*Coming soon*

Large groups of fruit flies (*D. melanogaster*)
*****************************************

Videos of 100 and 72 fruit flies. For the setup specifications check Supplementary Figure 4 in [1]_.

.. raw:: html

  <iframe width="280" height="160" style="padding:2px;border:2px solid white;" src="https://www.youtube.com/embed/X6jyW3gKzkc?ecver=1&rel=0&showinfo=01" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

.. raw:: html

  <iframe width="280" height="160" style="padding:2px;border:2px solid white;" src="https://www.youtube.com/embed/_M9xl4jBzVQ?ecver=1&rel=0&showinfo=01" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Small groups of fruit flies (*D. melanogaster*)
*****************************************

*Coming soon*

Small groups of ants
********************

*Coming soon*

Small groups of mice
********************

*Coming soon*

References
**********

.. [1] Romero-Ferrero, F., Bergomi, M.G., Hinz, R.C., Heras, F.J.H., de Polavieja, G.G., (2018). idtracker.ai: Tracking all individuals in large collectives of unmarked animals (submitted). (F.R.-F. and M.G.B. contributed equally to this work. Correspondence should be addressed to G.G.d.P: gonzalo.polavieja@neuro.fchampalimaud.org)
