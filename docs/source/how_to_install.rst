Installation and requirements
=============================

^^^^^^^^^^^^
Requirements
^^^^^^^^^^^^

The system has been developed and tested under the following specifications:

- Operating system: 64bit GNU/linux Mint 18.1
- CPU: Core(TM) i7-7700K CPU @4.20GHz   6 core Intel(R) Core(TM) i7-6800K CPU @3.40GHz
- GPU: Nvidia TITAN X or Nvidia GeForce GTX 1080 Ti
- RAM: 32Gb (for small groups) or 128Gb (for large groups)
- Disk: 1TB SSD

idtracker.ai is coded in python 2.7 and its core uses OpenCV 2 python wrappers
(version 2.13.4) and Tensorflow libraries (version 1.2.0). Due to the intense
use of deep neural networks, we recommend using a computer with a dedicated
NVIDA GPU supporting compute capability 3.0 or higher. Note that the parts of
the algorithm using Tensorflow libraries will run faster with a GPU. If a GPU
is not installed on the computer the CPU version of Tensorflow will be installed
but the speed of the tracking would be highly affected.

^^^^^^^^^^^^
Installation
^^^^^^^^^^^^

Go to our `Gitlab repository <https://gitlab.com/polavieja_lab/idtrackerai.git>`_
where you will find the installation instructions.
