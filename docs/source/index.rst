Welcome to idtracker.ai's documentation!
========================================

idtracker.ai allows to track groups of animals in videos recorded in laboratory conditions.

When using information from this web page please reference

  Romero-Ferrero, F., Bergomi, M.G., Hinz, R.C., Heras, F.J.H., de Polavieja, G.G., (2018).
  idtracker.ai: Tracking all individuals in large collectives of unmarked animals (submitted)
  (F.R.-F. and M.G.B. contributed equally to this work. Correspondence should be addressed to G.G.d.P:
  gonzalo.polavieja@neuro.fchampalimaud.org)

You can find the arXiv version in the following link: https://arxiv.org/abs/1803.04351.

The source code can be found in https://gitlab.com/polavieja_lab/idtrackerai.git. Report any problem by
opening an issue in the repository.

Should you need more information please contact us (idtrackerai@gmail.com).

Contents:
---------

.. toctree::
   :maxdepth: 2

   gallery
   how_to_install
   quickstart
   GUI_explained
   modules
   video_collectives_library
   individual_images_library

Documentation index and search
------------------------------

* :ref:`genindex`
* :ref:`search`
.. * :ref:`modindex`
