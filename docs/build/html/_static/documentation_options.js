var DOCUMENTATION_OPTIONS = {
    URL_ROOT: '',
    VERSION: '0.0.1',
    LANGUAGE: 'en',
    COLLAPSE_INDEX: false,
    FILE_SUFFIX: '.html',
    HAS_SOURCE: false,
    SOURCELINK_SUFFIX: '.txt'
};